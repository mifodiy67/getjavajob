import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {
        System.out.print("Введите слово : ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();

        char [] strCharArray = str.toCharArray ();

        System.out.println("Верно ли, что оно начинается и оканчивается на одну и ту же букву?");

        if (strCharArray[0] == strCharArray[strCharArray.length - 1]) {
            System.out.println("Yes");
        }
        else {
            System.out.println("No");
        }
    }
}
