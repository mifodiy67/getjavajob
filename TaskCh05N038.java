public class TaskCh05N038 {
    public static void main(String[] args) {
        int quant = 100;
        double distanceForHome = 0;
        double distanceForCross = 0;

        for (int i = 1; i <= quant; i++) {
            if (i % 2 == 1) {
                distanceForHome = distanceForHome + (double) 1/i;
            }
            else {
                distanceForHome = distanceForHome - (double) 1/i;
            }

            distanceForCross = distanceForCross + (double) 1/i;
        }

        System.out.print("Расстояние от дома : ");
        System.out.println(distanceForHome);
        System.out.print("Общий пройденный путь : ");
        System.out.println(distanceForCross);
    }
}
