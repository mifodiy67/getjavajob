import java.util.Scanner;

public class TaskCh10N043 {
    public static void main(String[] args) {
        System.out.print("Введите число : ");
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();

        SumNumber sumNumber = new SumNumber();
        CountNumber countNumber = new CountNumber();

        System.out.println("сумма цифр = " + sumNumber.sum(a));
        System.out.println("количества цифр = " + countNumber.count(a));
    }
}

class SumNumber {
    int sum(int a) {
        int result = 0;

        if(a < 10) return (result + a);

        result = (a % 10) + sum(a / 10);
        return result;
    }
}

class CountNumber {
    int countN = 0;
    int count(int a) {

        if(a < 10) return (++countN);

        countN++;
        count(a / 10);
        return countN;
    }
}

