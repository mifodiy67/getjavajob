import java.util.Scanner;

public class TaskCh05N064 {
    public static void main(String[] args) {
        int [] districtPopulation = new int[12];
        int [] districtSquare = new int[12];
        int sumPopulation = 0;
        int sumSquare = 0;
        double densityPeople = 0;

        Scanner in = new Scanner(System.in);

        for (int i = 0; i < 12; i++) {
            System.out.print("Введите кол-во жителей в " + (i + 1) + " районе : ");
            districtPopulation[i] = in.nextInt();

            System.out.print("Площадь " + (i + 1) + " района : ");
            districtSquare[i] = in.nextInt();
        }

        for (int i : districtPopulation) {
            sumPopulation += i;
        }

        for (int i : districtSquare) {
            sumSquare += i;
        }

        densityPeople = (double)sumPopulation/sumSquare;

        System.out.print("Средняя плотность населения в области : " + densityPeople);

    }
}
