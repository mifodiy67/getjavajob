import java.util.Scanner;

public class TaskCh10N044 {
    public static void main(String[] args) {

        System.out.print("Введите число : ");
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();

        DiitalRoot diitalRoot = new DiitalRoot();

        System.out.println("Цифровой корень данного числа = " + diitalRoot.root(a));
    }
}

class DiitalRoot {

    int root(int a) {
        int result = 0;

        if(a < 10) result = a;
        else {
            result = (a % 10) + root(a / 10);
            result = root(result);
        }

        return result;

    }
}