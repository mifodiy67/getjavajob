import java.util.Scanner;

public class TaskCh12N063 {
    static int n = 11;
    static int m = 4;
    static int[][] mas;

    public static void main(String[] args) {

        mas = new int[n][m];

        TaskCh12N063 taskCh12N063 = new TaskCh12N063();
        taskCh12N063.zapoln();
        taskCh12N063.averageLine();
    }

    void zapoln(){
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print("Введите число учеников в " + (i + 1) + " классе, " + (j + 1) + " класс в параллеле : ");
                Scanner in = new Scanner(System.in);
                mas[i][j] = in.nextInt();
            }
        }
    }

    void averageLine(){
        int sumParal = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                sumParal += mas[i][j];
            }

            double sredParal = (double) sumParal / m;

            System.out.printf("Среднее число учеников в " + (i + 1) + " классах : %.2f \n", sredParal);
            sumParal = 0;
        }
    }

}
