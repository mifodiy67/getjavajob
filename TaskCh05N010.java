import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        System.out.print("Текущий курс : ");
        Scanner in = new Scanner(System.in);
        int curse = in.nextInt();

        int[] tablUSD = new int[20];
        System.out.print("USD : ");
        for (int i = 0; i < 20; i++) {
            tablUSD[i] = i + 1;
            System.out.print(tablUSD[i] + " ");
        }
        System.out.println();

        int[] tablRUB = new int[20];
        System.out.print("RUB : ");
        for (int i = 0; i < 20; i++) {
            tablRUB[i] = tablUSD[i] * curse;
            System.out.print(tablRUB[i] + " ");
        }
    }
}
