import java.util.Scanner;

public class TaskCh11N245 {

    public static void main(String[] args) {
        System.out.print("Введите число элементов в массиве : ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int massive[] = new int[n];
        int massiveModern[] = new int[n];

        TaskCh11N245 taskCh11N245 = new TaskCh11N245();
        massive = taskCh11N245.zapoln(n);

        System.out.println("Введенный массив : ");
        taskCh11N245.vivod(massive);

        System.out.println("Массив вначале все отрицательные элементы : ");
        massiveModern = taskCh11N245.firstNegative(massive, massiveModern);
        taskCh11N245.vivod(massiveModern);

    }

    int[] zapoln (int n) {
        int mas[] = new int[n];
        System.out.println("Введите элементы массива : ");
        Scanner in = new Scanner(System.in);
        for(int i = 0; i < n; i++) {
            mas[i] = in.nextInt();
        }
        return mas;
    }

    void vivod (int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i]);
            if (i != (mas.length - 1)) {
                System.out.print(", ");
            }
            else System.out.println();
        }
    }

    int[] firstNegative (int masSource[], int masDrain[]) {
        int kStart = 0;
        int kEnd = 1;

        for (int i = 0; i < masSource.length; i++) {
            if (masSource[i] < 0) {
                masDrain[kStart] = masSource[i];
                kStart++;
            }
            else {
                masDrain[masSource.length - kEnd] = masSource[i];
                kEnd++;
            }
        }
        return masDrain;
    }
}
