import java.util.Scanner;

public class TaskCh02N013 {

    public static void main(String[] args) {
        System.out.print("Введите число 200 > n > 100 : ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int n100 = n / 100;
        int n10 = (n - (100 * n100)) / 10;
        int n1 = (n - (100 * n100) - (10 * n10));

        int nInv = (100 * n1) + (10 * n10) + (n100);
        System.out.printf("Инверсное число : %d", nInv);
    }

}
