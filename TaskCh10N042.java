import java.util.Scanner;

public class TaskCh10N042 {
    public static void main(String[] args) {
        System.out.print("Введите число : ");
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();

        System.out.print("Введите степень : ");
        int n = in.nextInt();

        Degree d = new Degree();

        System.out.println(a + " ^ " + n + " = " + d.deg(a, n));
    }
}

class Degree {
    int deg(int a, int n) {
        int result;

        if(n == 1) return a;
        result = a * deg(a, n - 1);
        return result;
    }
}

