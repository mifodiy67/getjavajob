public class TaskCh03N029 {
    public static void main(String[] args) {
        int X, Y, Z;
        boolean res;
        /**
         * а
         * каждое из чисел X и Y нечетное
         */
        res = X % 2 - Y % 2 == 0;

        /**
         * б
         * только одно из чисел X и Y меньше или равно 2
         */
        res = X <= 2 ^ Y <= 2;

        /**
         * в
         * хотя бы одно из чисел X и Y равно нулю
         */
        res = X == 0||Y == 0;

        /**
         * г
         * каждое из чисел X, Y, Z отрицательное
         */
        res = X < 0 & Y < 0 & Z < 0;

        /**
         * д
         * только одно из чисел X, Y и Z меньше или равно 3
         */
        res = (X <= 3 ^ Y <= 3 ^ Z <= 3) & !(X <= 3 & Y <= 3 & Z <= 3);

        /*
        X Y Z  res
        0 0 0   0
        0 0 1   1
        0 1 0   1
        0 1 1   0
        1 0 0   1
        1 0 1   0
        1 1 0   0
        1 1 1   0
        */

        /**
         * е
         * хотя бы одно из чисел X, Y, Z больше 100
         */
        res = X > 100 || Y > 100 || Z > 100;
    }
}