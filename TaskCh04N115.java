import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        System.out.print("Введите год : ");
        Scanner in = new Scanner(System.in);
        int year = in.nextInt();
        //int yearCicle = year - 1984;
        int yearCicle = year - 4;
                switch (yearCicle % 12) {
            case 0:
                System.out.print("Крыса");
                break;
            case 1:
                System.out.print("Корова");
                break;
            case 2:
                System.out.print("Тигр");
                break;
            case 3:
                System.out.print("Заяц");
                break;
            case 4:
                System.out.print("Дракон");
                break;
            case 5:
                System.out.print("Змея");
                break;
            case 6:
                System.out.print("Лошадь");
                break;
            case 7:
                System.out.print("Овца");
                break;
            case 8:
                System.out.print("Обезьяна");
                break;
            case -3:
            case 9:
                System.out.print("Петух");
                break;
            case -2:
            case 10:
                System.out.print("Собака");
                break;
            case -1:
            case 11:
                System.out.print("Свинья");
                break;
        }
        System.out.print(", ");
        switch (yearCicle % 10) {
            case 0:
            case 1:
                System.out.print("Зеленый");
                break;
            case 2:
            case 3:
                System.out.print("Красный");
                break;
            case 4:
            case 5:
                System.out.print("Желтый");
                break;
            case -3:
            case 6:
            case 7:
                System.out.print("Белый");
                break;
            case -2:
            case -1:
            case 8:
            case 9:
                System.out.print("Черный");
                break;
        }
    }
}
