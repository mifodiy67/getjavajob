import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        System.out.print("Введите число a : ");
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();

        System.out.print("Введите число b : ");
        int b = in.nextInt();

        int c = (a % b) * (b % a) + 1;
        System.out.printf("Ответ : %d", c);
    }
}
