import java.util.Calendar;
import java.util.GregorianCalendar;

public class TaskCh13N012 {

    static int n = 20;

    public static void main(String[] args) {

        TaskCh13N012 taskCh13N012 = new TaskCh13N012();
        Person mas [] = new Person[n];

        taskCh13N012.zapoln(mas);

        taskCh13N012.vivod(mas);

        System.out.println();
        System.out.println("Вывести сотрудников работающих более 3 лет : ");
        System.out.println();

        taskCh13N012.vivodJobMoreYear(mas);
    }

    void zapoln (Person mas []) {
        mas[0] = new Person("Иванов", "Иван", "Иваныч", "Москва", new GregorianCalendar(2010, 9, 9));
        mas[1] = new Person("Петров", "Петр", "Петрович", "Ростов", new GregorianCalendar(2011, 1, 1));
        mas[2] = new Person("Смирнов", "Александр", "Петрович", "Воронеж", new GregorianCalendar(2012, 1, 1));
        mas[3] = new Person("Кузнецов", "Денис", "Петрович", "Саратов", new GregorianCalendar(2013, 1, 1));
        mas[4] = new Person("Попов", "Сергей", "Петрович", "Тверь", new GregorianCalendar(2014, 1, 1));
        mas[5] = new Person("Васильев", "Андрей", "Петрович", "Тула", new GregorianCalendar(2015, 1, 1));
        mas[6] = new Person("Новиков", "Дмитрий", "Петрович", "Омск", new GregorianCalendar(2016, 1, 1));
        mas[7] = new Person("Морозов", "Максим", "Петрович", "Самара", new GregorianCalendar(2017, 1, 1));
        mas[8] = new Person("Волков", "Алексей", "Петрович", "Краснодар", new GregorianCalendar(2017, 7, 7));
        mas[9] = new Person("Егоров", "Артем", "Петрович", "Ульяновск", new GregorianCalendar(2017, 2, 2));
        mas[10] = new Person("Николаев", "Роман", "Петрович", "Томск", new GregorianCalendar(2016, 1, 1));
        mas[11] = new Person("Орлов", "Степан", "Петрович", "Тюмень", new GregorianCalendar(2015, 1, 1));
        mas[12] = new Person("Макаров", "Николай", "Петрович", "Владивосток", new GregorianCalendar(2014, 1, 1));
        mas[13] = new Person("Никитин", "Вадим", "Петрович", "Сургут", new GregorianCalendar(2013, 1, 1));
        mas[14] = new Person("Захаров", "Владимир", "Петрович", "Брянск", new GregorianCalendar(2012, 1, 1));
        mas[15] = new Person("Зайцев", "Тимофей", "Петрович", "Владимир", new GregorianCalendar(2011, 1, 1));
        mas[16] = new Person("Романов", "Антон", "Петрович", "Армавир", new GregorianCalendar(2010, 1, 1));
        mas[17] = new Person("Воробьев", "Кирилл", "Петрович", "Орел", new GregorianCalendar(2009, 1, 1));
        mas[18] = new Person("Фролов", "Евгений", "Петрович", "Казань", new GregorianCalendar(2008, 1, 1));
        mas[19] = new Person("Белов", "Валерий", "Петрович", "Чита", new GregorianCalendar(2007, 1, 1));

    }

    void vivod (Person mas []) {
        for (int i = 0; i < n; i++) {
            mas[i].show();
        }
    }

    void vivodJobMoreYear (Person mas []) {
        int dateJobMoreYear = 3;
        for (int i = 0; i < 20; i++) {
            mas[i].jobMoreYear(dateJobMoreYear);
        }
    }
}

class Person {
    String lastName;
    String firstName;
    String patronymic;
    String address;
    Calendar dateInJob;

    public Person(String lastName, String firstName, String patronymic, String address, Calendar dateInJob) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.address = address;
        this.dateInJob = dateInJob;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public Calendar getDateInJob() {
        return dateInJob;
    }

    public void jobMoreYear(int p) {
        Calendar todayDate = Calendar.getInstance();

        this.dateInJob.add(Calendar.YEAR, p);

        if (dateInJob.before(todayDate)) {
            this.showExceptDate();
        }
    }

    public void show() {
        System.out.print(getLastName() + " ");
        System.out.print(getFirstName() + " ");
        System.out.print(getPatronymic() + " ");
        System.out.print(getAddress() + " ");
        System.out.println(getDateInJob().getTime());
    }

    public void showExceptDate() {
        System.out.print(getLastName() + " ");
        System.out.print(getFirstName() + " ");
        System.out.print(getPatronymic() + " ");
        System.out.println(getAddress());
    }
}
