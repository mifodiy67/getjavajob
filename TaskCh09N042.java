import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        System.out.print("Введите слово : ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        StringBuilder strR = new StringBuilder(str);
        strR.reverse();
        System.out.print("Cлово наоборот : ");
        System.out.println(strR);
    }
}
