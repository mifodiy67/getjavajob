import java.util.Scanner;

public class TaskCh09N185 {
    public static void main(String[] args) {
        System.out.print("Введите арифметическое выражение : ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        int indexOpen = 0;
        int indexClose = 0;
        int indexNumClose;

        System.out.print("Правильно ли в нем рас-ставлены скобки : ");

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(') indexOpen++;
            if (str.charAt(i) == ')') indexClose++;
        }

        if (indexOpen == indexClose) {
            System.out.println("Да");
        }

        if (indexOpen < indexClose) {
            System.out.print("Нет, имеются лишние правые (закрывающие) скобки, позиция первой скобки : ");
            indexNumClose = str.indexOf(')');
            System.out.println(indexNumClose);
        }

        if (indexOpen > indexClose) {
            System.out.print("Нет, имеются лишние левые (открывающие) скобки, в количестве : ");
            System.out.println(indexOpen - indexClose);
        }
    }
}
