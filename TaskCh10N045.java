import java.util.Scanner;

public class TaskCh10N045 {
    public static void main(String[] args) {
        System.out.print("Введите первый член арифметической прогрессии : ");
        Scanner in = new Scanner(System.in);
        int a1 = in.nextInt();

        System.out.print("Введите разность арифметической прогрессии : ");
        int razn = in.nextInt();

        System.out.print("Введите номер искомого члена прогрессии : ");
        int n = in.nextInt();

        FindN findN = new FindN();
        FindSumN findSumN = new FindSumN();

        System.out.println(n + " член прогрессии = " + findN.find(a1, razn, n));
        System.out.println("Сумма " + n + " первых членов прогрессии = " + findSumN.findSum(a1, razn, n));
    }
}

class FindN {

    int find(int a, int b, int c) {
        if (c == 1) return a;
        return a + find(a, b, c - 1);
    }
}

class FindSumN {

    int findSum(int a, int b, int c) {
        if (c == 1) return a;
        return a + findSum(a + b, b, c - 1);
    }
}