import java.util.Scanner;

public class TaskCh10N049 {
    public static void main(String[] args) {
        System.out.print("Введите количество элементов n = ");
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();


        if (size == 0) System.out.println("Число элементов в массиве равно 0");
        else {
            int [] mas = new int[size];
            System.out.println("Введите элементы : ");
            for (int i = 0; i < size; i++) {
                mas[i] = in.nextInt();
            }

            FindIndMax findIndMax = new FindIndMax();
            findIndMax.massiv = mas;

            System.out.println("Индекс максимального элемента последовательности = " + findIndMax.find(size - 1));
        }
    }
}

class FindIndMax {
    int [] massiv;
    int maxInd = 0;


    int find(int n) {
        int max = massiv[n];
        if (n > 0) {
            max = massiv[find(n - 1)];
            if (massiv[n] > max) {
                max = massiv[n];
                maxInd = n;
            }
        }
        return maxInd;
    }
}