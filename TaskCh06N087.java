import java.util.Scanner;

public class TaskCh06N087 {

    public static void main(String[] args) {

        Game game = new Game();
        game.start();
    }
}

class Game {
    String team1, team2;
    int score1 = 0, score2 = 0;

    void start(){
        System.out.print("Enter team #1 : ");
        Scanner in = new Scanner(System.in);
        team1 = in.nextLine();

        System.out.print("Enter team #2 : ");
        team2 = in.nextLine();

        play();
    }


    void play(){
        System.out.print("Enter team to score (1 or 2 or 0 to finish game) : ");
        Scanner in = new Scanner(System.in);
        int team = in.nextInt();

        if (team == 0) {
            System.out.println(result());

        }
        else {
            System.out.print("Enter score (1 or 2 or 3) : ");
            int score = in.nextInt();

            if (team == 1) {
                score1 = score1 + score;
                System.out.println(score());
            } else if (team == 2) {
                score2 = score2 + score;
                System.out.println(score());
            }

            play();
        }
    }

    String score() {
        return score1 + " : " + score2;
    }

    String result(){
        String res = "";
        if (score1 > score2) {
            res = "Winner " + team1 + " score " + score1 +  " loser " + team2 + " score " + score2;
        }
        else if (score2 > score1) {
            res = "Winner " + team2 + " score " + score2 +  " loser " + team1 + " score " + score1;
        }
        else if (score1 == score2) {
            res = "The draw " + team1 + " score " + score1 +  " " + team2 + " score " + score2;
        }

        return res;
    }
}
