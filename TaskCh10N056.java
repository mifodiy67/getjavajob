import java.util.Scanner;

public class TaskCh10N056 {
    public static void main(String[] args) {
        System.out.print("Введите натуральное число больше единице : ");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();

        TaskCh10N056 taskCh10N056 = new TaskCh10N056();
        if (number > 1) {
            System.out.println(taskCh10N056.defindSimpleNumber(number, 2) ? "Число простое" : "Число не простое");
        }
        else {
            System.out.println("Число не подлежит проверке");
        }
    }

    boolean defindSimpleNumber (int n, int i){
        if (i > Math.sqrt(n)) return  true;
        if (n % i == 0) return false;

        return defindSimpleNumber(n, i + 1);
    }

}
