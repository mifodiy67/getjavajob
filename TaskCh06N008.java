import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String[] args) {
        System.out.print("Введите число : ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int sqrtN = (int) Math.sqrt(n);

        for (int i = 1; i <= sqrtN; i++){
            System.out.print(i*i);
            if (i < sqrtN) System.out.print(", ");
        }
    }
}
