import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        int indexA = 0, indexO = 0;
        System.out.print("Введите слово латиницей : ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        StringBuilder sb = new StringBuilder(str);

        indexA = sb.indexOf("a");
        indexO = sb.lastIndexOf("o");

        if ((indexA == -1) || (indexO == -1)) {
            System.out.print("Буквы a и o - не встречаются одновременно");
        }
        else {
            sb.setCharAt(indexA, 'o');
            sb.setCharAt(indexO, 'a');

            System.out.print("Измененное слово : ");
            System.out.println(sb);
        }
    }
}
