import java.util.Scanner;

public class TaskCh10N050 {
    public static void main(String[] args) {
        System.out.print("Введите число n = ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        System.out.print("Введите число m = ");
        int m = in.nextInt();

        AckFun ackFun = new AckFun();

        System.out.println(ackFun.ack(n, m));
    }
}

class AckFun {

    int ack(int n, int m) {
        if (n == 0) {
            return m + 1;
        }
        else if ((n != 0) & (m == 0)) {
            return ack(n - 1, 1);
        }
        else return ack(n - 1, ack(n, m - 1));
    }
}
