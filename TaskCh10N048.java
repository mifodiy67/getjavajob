import java.util.Scanner;

public class TaskCh10N048 {
    public static void main(String[] args) {
        System.out.print("Введите количество элементов n = ");
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();


        if (size == 0) System.out.println("Число элементов в массиве равно 0");
        else {
            int [] mas = new int[size];
            System.out.println("Введите элементы : ");
            for (int i = 0; i < size; i++) {
                mas[i] = in.nextInt();
            }

            FindMax findMax = new FindMax();
            findMax.massiv = mas;

            System.out.println("Максимальный элемент последовательности = " + findMax.find(size - 1));
        }
    }
}

class FindMax {
    int [] massiv;

    int find(int n) {
        int max = massiv[n];
        if (n > 0) {
            max = find(n - 1);
            if (massiv[n] > max) max = massiv[n];
        }
        return max;
    }
}