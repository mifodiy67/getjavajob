import java.util.Scanner;

public class TaskCh02N039 {
    public static void main(String[] args) {
        System.out.print("Введите число 0 < h <= 23 : ");
        Scanner in = new Scanner(System.in);
        int h = in.nextInt();

        System.out.print("Введите число 0 <= m <= 59 : ");
        int m = in.nextInt();

        System.out.print("Введите число 0 <= s <= 59 : ");
        int s = in.nextInt();

        double angleOfOneHouse = 360 / 12;
        double angleOfOneMinute = angleOfOneHouse / 60;
        double angleOfOneSecunde = angleOfOneMinute / 60;

        double n = (angleOfOneHouse * h) + (angleOfOneMinute * m) + (angleOfOneSecunde * s);

        if (h >= 12) n = (n - 360);

        System.out.printf("Инверсное число : %f", n);
    }
}
