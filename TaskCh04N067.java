import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        System.out.print("Введите число дней : ");
        Scanner in = new Scanner(System.in);
        int day = in.nextInt();

        if (((day % 7) < 6) & ((day % 7) > 0)) System.out.println("Workday");
        else System.out.println("Weekend");
    }
}
