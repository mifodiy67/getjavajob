import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String[] args) {
        System.out.print("Введите натуральное число : ");
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();

        System.out.print("Верно ли, что оно заканчивается четной цифрой : ");
        if (num % 2 == 0) System.out.println("Да");
        else System.out.println("Нет");

        System.out.print("Верно ли, что оно заканчивается нечетной цифрой : ");
        if (num % 2 == 1) System.out.println("Да");
        else System.out.println("Нет");
    }
}
