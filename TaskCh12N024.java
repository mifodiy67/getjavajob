import java.util.Scanner;

public class TaskCh12N024 {
    static int n;

    public static void main(String[] args) {

        int[][] masA, masB;

        System.out.print("Введите число элементов в массиве : ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();

        TaskCh12N024 taskCh12N024 = new TaskCh12N024();

        masA = taskCh12N024.zapolnTriangelPascal(n);

        System.out.println("Массив а)");
        taskCh12N024.vivod(masA);
        System.out.println();

        masB = taskCh12N024.zapolnMoveString(n);

        System.out.println("Массив б)");
        taskCh12N024.vivod(masB);

    }

    int[][] zapolnTriangelPascal (int n) {
        int mas[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i == 0) | (j == 0)) mas[i][j] = 1;
                else mas[i][j] = mas[i - 1][j] + mas[i][j - 1];
            }
        }
        return mas;
    }

    int[][] zapolnMoveString (int n) {
        int mas[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                mas[i][j] = i + j + 1;
                if (i + j >= n) mas[i][j] = mas[i][j] - n;
            }
        }
        return mas;
    }

    void vivod (int[][] mas) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

}
