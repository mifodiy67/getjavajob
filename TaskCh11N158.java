import java.util.Scanner;

public class TaskCh11N158 {

    public static void main(String[] args) {
        System.out.print("Введите число элементов в массиве : ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int massive[] = new int[n];

        TaskCh11N158 taskCh11N158 = new TaskCh11N158();
        massive = taskCh11N158.zapoln(n);

        System.out.println("Введенный массив : ");
        taskCh11N158.vivod(massive);

        System.out.println("Массив без повотряющихся элементов : ");
        taskCh11N158.vivod(taskCh11N158.deleteRepeat(massive));

    }

    int[] zapoln (int n) {
        int mas[] = new int[n];
        System.out.println("Введите элементы массива : ");
        Scanner in = new Scanner(System.in);
        for(int i = 0; i < n; i++) {
            mas[i] = in.nextInt();
        }
        return mas;
    }

    void vivod (int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i]);
            if (i != (mas.length - 1)) {
                System.out.print(", ");
            }
            else System.out.println();
        }
    }

    int[] deleteRepeat (int mas[]) {

        for (int i = 0; i < mas.length - 1; i++) {
            int element = mas[i];
            for (int j = i + 1; j < mas.length; j++){
                if (mas[j] == element) {
                    mas = deleteElement(mas, j);
                }
            }
        }
        return mas;
    }

    int[] deleteElement (int mas[], int index) {
        int newMas[] = new int[mas.length - 1];

        for (int i = 0; i < index; i++) {
            newMas[i] = mas[i];
        }

        for (int i = index; i < mas.length - 1; i++) {
            newMas[i] = mas[i + 1];
        }
        return newMas;
    }

}
