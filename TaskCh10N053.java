import java.util.Scanner;

public class TaskCh10N053 {
    int[] array = new int[100];
    int i = 0;

    public static void main(String[] args) {
        System.out.println("Введите числа : ");

        TaskCh10N053 taskCh10N053 = new TaskCh10N053();
        taskCh10N053.vvod();
        taskCh10N053.vivod();
    }

    void vvod(){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n != 0) {
            array[i++] = n;
            vvod();
        }
        else i--;
    }

    void vivod(){
        if (i >= 0) {
            System.out.print(array[i--] + ", ");
            vivod();
        }
    }
}