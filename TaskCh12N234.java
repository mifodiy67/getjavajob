import java.util.Scanner;

public class TaskCh12N234 {
    static int [][] mas;
    static int n, m;

    public static void main(String[] args) {
        System.out.print("Введите число строк массива : ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();

        System.out.print("Введите число столбцов массива : ");
        m = in.nextInt();

        mas = new int[n][m];

        TaskCh12N234 taskCh12N234 = new TaskCh12N234();
        taskCh12N234.zapoln();
        taskCh12N234.vivod();

        System.out.print("Введите номер строки, которую удалить : ");
        int k = in.nextInt();

        taskCh12N234.deleteLine(k);
        taskCh12N234.vivod();

        System.out.print("Введите номер столбца, который удалить : ");
        int s = in.nextInt();

        taskCh12N234.deleteColumn(s);
        taskCh12N234.vivod();
    }

    void zapoln () {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mas[i][j] = (int) (Math.random() * 100);
            }
        }
    }

    void vivod () {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (mas[i][j] < 10) System.out.print(" ");
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

    void deleteLine (int k) {
        for (int i = k - 1; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (i == n - 1) mas[i][j] = 0;
                else mas[i][j] = mas[i + 1][j];
            }
        }
    }

    void deleteColumn (int s) {
        for (int i = 0; i < n; i++) {
            for (int j = s - 1; j < m; j++) {
                if (j == m - 1) mas[i][j] = 0;
                else mas[i][j] = mas[i][j + 1];
            }
        }
    }
}
