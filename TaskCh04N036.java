import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        System.out.print("Введите вещественное число : ");
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();

        if ((t % 5) < 3) System.out.println("green ");
        else System.out.println("red ");
    }
}
