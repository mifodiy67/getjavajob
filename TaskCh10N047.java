import java.util.Scanner;

public class TaskCh10N047 {
    public static void main(String[] args) {
        System.out.print("Введите номер искомого члена последовательности Фибоначчи : ");
        Scanner in = new Scanner(System.in);
        int k = in.nextInt();

        FindK findK = new FindK();

        System.out.println(k + " член последовательности = " + findK.find(k));
    }
}

class FindK {

    int find(int k) {
        if ((k == 1)||(k == 2)) return 1;
        return find(k - 1) + find(k - 2);
    }
}