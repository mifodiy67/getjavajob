public class TaskOOP {
    public static void main(String[] args) {
        Candidate candidate [] = new Candidate[10];

        candidate[0] = new CandidateGetJavaJob("Сергей");
        candidate[1] = new CandidateSelfLearner("Тимур");
        candidate[2] = new CandidateSelfLearner("Миша");
        candidate[3] = new CandidateSelfLearner("Александр");
        candidate[4] = new CandidateSelfLearner("Владимир");
        candidate[5] = new CandidateGetJavaJob("Олег");
        candidate[6] = new CandidateSelfLearner("Андрей");
        candidate[7] = new CandidateSelfLearner("Кирилл");
        candidate[8] = new CandidateSelfLearner("Никита");
        candidate[9] = new CandidateSelfLearner("Денис");

        System.out.println("Привет, представьтесь и опишите свои навыки в программировании на Java," +
                "пожалуйста.");

        for (int i = 0; i < 10; i++) {
            System.out.println();
            candidate[i].helloMessage();
            candidate[i].myExperienseMessage();
        }
    }
}

abstract class Candidate implements welcomeCandidate{
    String name;

    Candidate(String n) {
        name = n;
    }

    abstract String canIDo();

    @Override
    public void helloMessage() {
        System.out.println("Привет, мое имя " + getName() + ".");
    }

    @Override
    public void myExperienseMessage() {
        System.out.println(canIDo());
    }

    String getName() {
        return name;
    }
}

class CandidateGetJavaJob extends Candidate {

    public CandidateGetJavaJob(String name) {
        super(name);
    }

    @Override
    String canIDo() {
        return "Я успешно прошел экзамены getJavaJob и обзоры кода.";
    }
}

class CandidateSelfLearner extends Candidate {

    public CandidateSelfLearner(String name) {
        super(name);
    }

    @Override
    String canIDo() {
        return "Я сам изучал Java, никто не проверял мои знания и грамотность моего кода.";
    }
}

interface welcomeCandidate {
    void helloMessage();
    void myExperienseMessage();
}