import java.util.Scanner;

public class TaskCh10N052 {
    public static void main(String[] args) {
        System.out.print("Введите число = ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        ZerkNum zerkNum = new ZerkNum();

        zerkNum.zerk(n);
    }
}

class ZerkNum {

    void zerk(int n) {
        if (n > 10) {
            System.out.print(n % 10);
            zerk(n / 10);
        }
        else System.out.print(n);
    }
}