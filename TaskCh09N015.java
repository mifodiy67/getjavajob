import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {
        System.out.print("Введите слово : ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();

        System.out.print("Введите номер символа : ");
        int k = in.nextInt();

        char [] strCharArray = str.toCharArray ();

        System.out.println("Символ пд номером " + k + " в слове " + str + " : " + strCharArray[k - 1]);

    }
}
