import java.util.Scanner;

public class TaskCh10N055 {
    static char[]simbol = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    static int[]massiv = new int[32];
    static int k = 0;

    public static void main(String[] args) {


        System.out.print("Введите число : ");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();

        System.out.print("Введите разрядность системы счисления : ");
        int sizeNumber = in.nextInt();

        TaskCh10N055 taskCh10N055 = new TaskCh10N055();
        taskCh10N055.decToN(number, sizeNumber);

        for(int i = k; i >= 0; i--) {
            System.out.print(simbol[massiv[i]]);
        }

    }

    void decToN(int n, int size) {
        if (n < size) {
            massiv[k] = n;
        }
        else {
            massiv[k++] = n % size;
            decToN(n / size, size);
        }


    }
}
