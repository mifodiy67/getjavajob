import java.util.Scanner;

public class TaskCh12N028 {

    static int n;
    static int[][] mas;

    public static void main(String[] args) {

        System.out.print("Введите размерность массива : ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();

        TaskCh12N028 taskCh12N028 = new TaskCh12N028();

        mas = taskCh12N028.zapoln(n);
        taskCh12N028.vivod(mas);
        System.out.println();
    }

    int[][] zapoln (int n) {
        int mas[][] = new int[n][n];
        int startNum = 1;

        int lengthLine = n;

        int numCenter = (int) Math.ceil(n / 2.0);

        for (int i = 0; i < numCenter; i++) {

            // do top side
            for (int j = 0; j < lengthLine; j++) {
                mas[i][i + j] = startNum++;
            }

            // do right side
            for (int j = 1; j < lengthLine; j++) {
                mas[i + j][n - 1 - i] = startNum++;
            }

            // do bottom side
            for (int j = lengthLine - 2; j > -1; j--) {
                mas[n - 1 - i][i + j] = startNum++;
            }

            // do left side
            for (int j = lengthLine - 2; j > 0; j--) {
                mas[i + j][i] = startNum++;
            }

            lengthLine = lengthLine - 2;

        }
        return mas;
    }

    void vivod (int[][] mas) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }
}
