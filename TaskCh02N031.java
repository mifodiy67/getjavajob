import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        System.out.print("Введите число 100 <= n <= 999 : ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int n100 = n / 100;
        int n10 = (n - (100 * n100)) / 10;
        int n1 = (n - (100 * n100) - (10 * n10));

        int x = (100 * n100) + (10 * n1) + (n10);
        System.out.printf("Искомое число x : %d", x);
    }
}
