import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String[] args) {
        System.out.print("Введите число : ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Factorial f = new Factorial();

        System.out.println(n + "! = " + f.fact(n));
    }
}

class Factorial {
    int fact(int n) {
        int result;

        if(n==1) return 1;
        result = fact(n-1) * n;
        return result;
    }
}