import java.util.Scanner;

public class TaskCh12N023 {
    static int n;

    public static void main(String[] args) {
        int[][] masA, masB, masC;
        int[][] masBplus, masCplus;

        System.out.print("Введите число элементов в массиве : ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();

        TaskCh12N023 taskCh12N023 = new TaskCh12N023();

        masA = taskCh12N023.zapolnA(n);

        System.out.println("Массив а)");
        taskCh12N023.vivod(masA);
        System.out.println();

        masB = new int[n][];
        System.arraycopy(masA, 0 , masB, 0, n);
        masBplus = taskCh12N023.zapolnB(n);
        masB = taskCh12N023.sum(masB, masBplus);

        System.out.println("Массив б)");
        taskCh12N023.vivod(masB);
        System.out.println();

        masC = taskCh12N023.zapolnC(n);
        System.out.println("Массив в)");
        taskCh12N023.vivod(masC);
        System.out.println();

    }

    int[][] zapolnA (int n) {
        int mas[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) mas[i][j] = 1;
                else if (j == n - i - 1) mas[i][j] = 1;
                else mas[i][j] = 0;
            }
        }
        return mas;
    }

    int[][] zapolnB (int n) {
        int mas[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == (n -1) / 2) mas[i][j] = 1;
                else if (j == (n - 1) / 2) mas[i][j] = 1;
                else mas[i][j] = 0;
            }
        }
        return mas;
    }

    int[][] zapolnC (int n) {
        int mas[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((j >= i) & (j < (n - i))) mas[i][j] = 1;
                else if ((j <= i) & (j >= (n - i - 1))) mas[i][j] = 1;
                else mas[i][j] = 0;
            }
        }
        return mas;
    }

    void vivod (int[][] mas) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

    int[][] sum (int[][] mas1, int[][] mas2) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                mas1[i][j] = mas1[i][j] | mas2[i][j];
            }
        }
        return mas1;
    }
}
