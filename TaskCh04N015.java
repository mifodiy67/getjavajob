import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        System.out.print("Сегодняшняя дата : ");
        Scanner in = new Scanner(System.in);
        String in1 = in.nextLine();

        System.out.print("Дата рождения : ");
        String in2 = in.nextLine();

        String[] in1Mas = in1.split(" ");
        String[] in2Mas = in2.split(" ");

        int[] in1MasInt = new int[in1Mas.length];
        int[] in2MasInt = new int[in2Mas.length];

        for (int i = 0; i < in1Mas.length; i++) in1MasInt[i] = Integer.parseInt(in1Mas[i]);
        for (int i = 0; i < in2Mas.length; i++) in2MasInt[i] = Integer.parseInt(in2Mas[i]);

        int old = in1MasInt[1] - in2MasInt[1];

        if ((in1MasInt[0] - in2MasInt[0]) < 0) old = old - 1;

        System.out.println(old);
    }

}
