import java.util.Scanner;

public class TaskCh12N025 {

    static int n = 12;
    static int m = 10;
    static int[][] mas;

    public static void main(String[] args) {

        TaskCh12N025 taskCh12N025 = new TaskCh12N025();

        mas = taskCh12N025.zapolnA();
        System.out.println("Массив а)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnB();
        System.out.println("Массив б)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnC();
        System.out.println("Массив в)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnD();
        System.out.println("Массив г)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnE();
        System.out.println("Массив д)");
        taskCh12N025.vivod(mas, m, n);
        System.out.println();

        mas = taskCh12N025.zapolnF();
        System.out.println("Массив е)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnG();
        System.out.println("Массив ж)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnH();
        System.out.println("Массив з)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnI();
        System.out.println("Массив и)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnK();
        System.out.println("Массив к)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnL();
        System.out.println("Массив л)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnM();
        System.out.println("Массив м)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnN();
        System.out.println("Массив н)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnO();
        System.out.println("Массив о)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnP();
        System.out.println("Массив п)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

        mas = taskCh12N025.zapolnR();
        System.out.println("Массив р)");
        taskCh12N025.vivod(mas, n, m);
        System.out.println();

    }

    int[][] zapolnA () {
        int mas[][] = new int[n][m];
        int k = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mas[i][j] = k++;
            }
        }
        return mas;
    }

    int[][] zapolnB () {
        int mas[][] = new int[n][m];

        for (int i = 0; i < n; i++) {
            int k = 1 + i;
            for (int j = 0; j < m; j++) {
                mas[i][j] = k;
                k = k + 12;
            }
        }
        return mas;
    }

    int[][] zapolnC () {
        int mas[][] = new int[n][m];

        for (int i = 0; i < n; i++) {
            int k = 10 * (i + 1);
            for (int j = 0; j < m; j++) {
                mas[i][j] = k--;
            }
        }
        return mas;
    }

    int[][] zapolnD () {
        int mas[][] = new int[n][m];

        for (int i = 0; i < n; i++) {
            int k = 12 - i;
            for (int j = 0; j < m; j++) {
                mas[i][j] = k;
                k = k + 12;
            }
        }
        return mas;
    }

    int[][] zapolnE () {
        int mas[][] = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 == 1) mas[i][j] = (i + 1) * n - j;
                else mas[i][j] = i * n + j + 1;
            }
        }
        return mas;
    }

    int[][] zapolnF () {
        int mas[][] = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (j % 2 == 1) mas[i][j] = (j + 1) * n - i;
                else mas[i][j] = j * n + i + 1;
            }
        }
        return mas;
    }

    int[][] zapolnG () {
        int mas[][] = new int[n][m];
        int k = 111;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mas[i][j] = k + j;

            }
            k = k - 10;
        }
        return mas;
    }

    int[][] zapolnH () {
        int mas[][] = new int[n][m];
        int k = 109;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mas[i][j] = k - 12 * j;

            }
            k = k + 1;
        }
        return mas;
    }

    int[][] zapolnI () {
        int mas[][] = new int[n][m];
        int k = 120;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mas[i][j] = k - j;

            }
            k = k - 10;
        }
        return mas;
    }

    int[][] zapolnK () {
        int mas[][] = new int[n][m];
        int k = 120;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                mas[i][j] = k - 12 * j;

            }
            k = k - 1;
        }
        return mas;
    }

    int[][] zapolnL () {
        int mas[][] = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (i % 2 == 1) mas[i][j] = (n - i) * m - (m - j) + 1;
                else mas[i][j] = (n - i) * m - j;
            }
        }
        return mas;
    }

    int[][] zapolnM () {
        int mas[][] = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (i % 2 == 1) mas[i][j] = (i + 1) * m - (m - j - 1);
                else mas[i][j] = (i + 1) * m - j;
            }
        }
        return mas;
    }

    int[][] zapolnN () {
        int mas[][] = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (j % 2 == 1) mas[i][j] = m * n - n * j - (n - i - 1);
                else mas[i][j] = m * n - i - n * j;
            }
        }
        return mas;
    }

    int[][] zapolnO () {
        int mas[][] = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (j % 2 == 1) mas[i][j] = n * j + i + 1;
                else mas[i][j] = n - i + n * j;
            }
        }
        return mas;
    }

    int[][] zapolnP () {
        int mas[][] = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (i % 2 == 1) mas[i][j] = m * (n - i) - j;
                else mas[i][j] = m * (n - i) - (m - j - 1);
            }
        }
        return mas;
    }

    int[][] zapolnR () {
        int mas[][] = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (j % 2 == 1) mas[i][j] = n * (m - j - 1) + (n - i);
                else mas[i][j] = n * (m - j - 1) + i + 1;
            }
        }
        return mas;
    }

    void vivod (int[][] mas, int n, int m) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

}
