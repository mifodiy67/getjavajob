public class TaskCh10N051 {
    public static void main(String[] args) {
        Proc1 proc1 = new Proc1();
        Proc2 proc2 = new Proc2();
        Proc3 proc3 = new Proc3();
        System.out.println("а)");
        proc1.pro(5);
        System.out.println();
        System.out.println("б)");
        proc2.pro(5);
        System.out.println();
        System.out.println("в)");
        proc3.pro(5);
    }
}

class Proc1 {
    void pro(int n) {
        if (n > 0) {
            System.out.println(n);
            pro(n - 1);
        }
        return;
    }
}

class Proc2 {
    void pro(int n) {
        if (n > 0) {
            pro(n - 1);
            System.out.println(n);
        }
        return;
    }
}

class Proc3 {
    void pro(int n) {
        if (n > 0) {
            System.out.println(n);
            pro(n - 1);
            System.out.println(n);
        }
        return;
    }
}